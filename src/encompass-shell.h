#ifndef _ENCOMPASS_SHELL_H_
#define _ENCOMPASS_SHELL_H_

typedef struct _EShellPrivate EShellPrivate;

typedef struct {
  BonoboObject parent;

  EShellPrivate * priv;
} EShell;

typedef struct {
  BonoboObjectClass parent_class;

  POA_GNOME_Encompass_Shell__epv * epv;
} EShellClass;

#define ENCOMPASS_TYPE_SHELL (encompass_shell_get_type())
#define ENCOMPASS_SHELL(obj) (GTK_CHECK_CAST ((obj), ENCOMPASS_TYPE_SHELL, EShell))
#define ENCOMPASS_SHELL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), ENCOMPASS_TYPE_SHELL, EShellClass))
#define ENCOMPASS_IS_SHELL(obj)     (GTK_CHECK_TYPE ((obj), ENCOMPASS_TYPE_SHELL))
#define ENCOMPASS_IS_SHELL_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((obj), ENCOMPASS_TYPE_SHELL))

GtkType encompass_shell_get_type (void);
gboolean encompass_shell_construct (EShell * eshell,
				    const gchar * iid);
EShell * encompass_shell_new (void);

#endif
