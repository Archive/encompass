#ifndef _ENCOMPASS_H_
#define _ENCOMPASS_H_

#include <glib.h>
#include <gnome.h>
#include <bonobo.h>
#include <bonobo-activation/bonobo-activation.h>
#include <libbonobo.h>

#undef PACKAGE
#undef VERSION

#include <config.h>

#include "Encompass.h"

#include "encompass-shell.h"

#endif
