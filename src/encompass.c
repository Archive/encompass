#include "encompass.h"

static gint num_wins = 0;
static GNOME_Encompass_Shell * shell = NULL;

static gint encompass_idle_cb (void * data) {
  GSList * uri_list = NULL;
  GNOME_Encompass_Shell corba_shell;
  CORBA_Environment ev;

  CORBA_exception_init (&ev);

  uri_list = (GSList *) data;

  shell = encompass_shell_new ();
  if (shell == NULL) {
    corba_shell =
      bonobo_activation_activate_from_id ("OAFIID:GNOME_Encompass_Shell",
					  0, NULL, &ev);
    if (ev._major != CORBA_NO_EXCEPTION || corba_shell == CORBA_OBJECT_NIL) {
      CORBA_exception_free (&ev);
      g_error ("Couldn't initialize the Encompass Shell");
      bonobo_main_quit ();
      return FALSE;
    }
  } else {
    corba_shell = bonobo_object_corba_objref (BONOBO_OBJECT (shell));
    Bonobo_Unknown_ref (corba_shell, &ev);
  }

  if (uri_list == NULL) {
    GNOME_Encompass_Shell_handleURI (corba_shell, "", &ev);
  } else {
    GSList * p;

    for (p = uri_list; p != NULL; p = p->next) {
      GNOME_Encompass_Shell_handleURI (corba_shell, (gchar *) p->data, &ev);
    }
    g_slist_free (p);
  }

  g_slist_free (uri_list);
  CORBA_exception_free (&ev);

  if (shell == NULL) {
    bonobo_main_quit ();
  }

  return FALSE;
}

gint main (gint argc, gchar *argv[]) {

  CORBA_Environment ev;
  CORBA_ORB orb;
  GError * gconf_error = NULL;
  GnomeProgram * encompass;
  GSList * uri_list = NULL;
  poptContext ctx;
  const gchar ** args;
  GValue context = { 0 };

  CORBA_exception_init (&ev);

#ifdef ENABLE_NLS
  bindtextdomain (PACKAGE, GNOME_LOCALE_DIR);
  bind_textdomain_codeset (PACKAGE, "UTF-8");
  textdomain (PACKAGE);
#endif
  
  encompass = gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
				  argc, argv, GNOME_PARAM_POPT_TABLE,
				  NULL, NULL);

  g_object_get_property (G_OBJECT (encompass), GNOME_PARAM_POPT_CONTEXT,
			 g_value_init (&context, G_TYPE_POINTER));
  ctx = g_value_get_pointer (&context);

  args = poptGetArgs (ctx);

  if (!bonobo_activation_is_initialized ()) {
    orb = bonobo_activation_init (argc, argv);
  } else {
    orb = bonobo_activation_orb_get ();
  }

  if (!gconf_init (argc, argv, &gconf_error)) {
    g_assert (gconf_error != NULL);
    g_error ("GConf init failed:\n  %s", gconf_error->message);
    return FALSE;
  }

  gnome_vfs_init ();

  args = poptGetArgs (ctx);
  if (args != NULL) {
    const gchar ** p;

    for (p = args; *p != NULL; p++) {
      uri_list = g_slist_prepend (uri_list, (char *) *p);
    }
  }

  poptFreeContext (ctx);

  g_idle_add (encompass_idle_cb, uri_list);
  bonobo_main ();

  CORBA_exception_free (&ev);
  return 0;
}
