#include "encompass.h"

void encompass_create_window (const gchar * uri) {
  BonoboUIComponent * ui_component;
  BonoboUIContainer * ui_container;
  GtkWidget * window;

  window = bonobo_window_new ("Encompass", "Encompass");
  ui_container = bonobo_window_get_ui_container (BONOBO_WINDOW (window));
  ui_component = bonobo_ui_component_new ("Encompass Shell");

  bonobo_ui_component_set_container (ui_component,
				     BONOBO_OBJREF (ui_container), NULL);

  bonobo_ui_util_set_ui (ui_component, NULL, "encompass.xml",
			 "encompass", NULL);

  gtk_widget_show (window);
  if (uri != NULL && *uri) {
    printf ("DEBUG: %s\n", uri);
  }
}
