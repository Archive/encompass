#include "encompass.h"

#define PARENT_TYPE bonobo_object_get_type ()

struct _EShellPrivate {
  gchar * iid;
};

static void impl_Shell_handleURI (PortableServer_Servant servant,
				  const CORBA_char * uri,
				  CORBA_Environment * ev) {
  EShell * eshell;
  EShellPrivate * priv;

  eshell = ENCOMPASS_SHELL (bonobo_object_from_servant (servant));
  priv = eshell->priv;

  encompass_create_window ((gchar *) uri);
}

BONOBO_CLASS_BOILERPLATE_FULL (EShell, encompass_shell,
			       GNOME_Encompass_Shell, BonoboObject,
			       BONOBO_OBJECT_TYPE)

static void destroy (GtkObject * object) {
  EShell * eshell;
  EShellPrivate * priv;

  eshell = ENCOMPASS_SHELL (object);
  priv = eshell->priv;
  if (priv->iid != NULL) {
    bonobo_activation_active_server_unregister (priv->iid,
						bonobo_object_corba_objref (BONOBO_OBJECT (eshell)));
  }
  g_free (priv);
}

static void encompass_shell_class_init (EShellClass * klass) {
  BonoboObjectClass * object_class;
  POA_GNOME_Encompass_Shell__epv * epv;

  parent_class = gtk_type_class (PARENT_TYPE);

  object_class = BONOBO_OBJECT_CLASS (klass);

  epv = &klass->epv;
  epv->handleURI = impl_Shell_handleURI;
}

static void encompass_shell_instance_init (EShell * eshell) {
  EShellPrivate *priv;

  priv = g_new0 (EShellPrivate, 1);
  priv->iid = NULL;
  eshell->priv = priv;
}

gboolean encompass_shell_construct (EShell * eshell,
				    const gchar * iid) {
  EShellPrivate * priv;
  CORBA_Object corba_object;
  CORBA_Environment ev;

  g_return_val_if_fail (eshell != NULL, FALSE);

  corba_object = bonobo_object_corba_objref (BONOBO_OBJECT (eshell));
  if (bonobo_activation_active_server_register (iid, corba_object) !=
      Bonobo_ACTIVATION_REG_SUCCESS) {
    return FALSE;
  }

  while (gtk_events_pending ()) {
    gtk_main_iteration ();
  }

  priv = eshell->priv;

  priv->iid = g_strdup (iid);

  return TRUE;
}

EShell * encompass_shell_new (void) {
  EShell * new;
  EShellPrivate * priv;

  new = g_object_new (encompass_shell_get_type (), NULL);

  if (!encompass_shell_construct (new, "OAFIID:GNOME_Encompass_Shell")) {
    bonobo_object_unref (BONOBO_OBJECT (new));
    return NULL;
  }

  priv = new->priv;

  return new;
}
